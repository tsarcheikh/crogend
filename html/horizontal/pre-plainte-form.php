<?php
        include 'connexion.php' ;
        
            @$nature_plainte = $_POST['nature_plainte'] ;
            @$date_faits = $_POST['date_faits'] ;
            @$sexe = $_POST['sexe'] ;
            @$qualite_plainte = $_POST['qualite_plainte'] ;
            @$prenom = $_POST['prenom'] ;
            @$nom = $_POST['nom'] ;
            @$situation_familliale = $_POST['situation_familliale'] ; 
            @$epoux = $_POST['epoux'] ;
            @$date_naissance = $_POST['date_naissance'] ;
            @$lieu_naissance = $_POST['lieu_naissance'] ;
            @$adresse = $_POST['adresse'] ;
            @$region = $_POST['region'] ;
            @$departement = $_POST['departement'] ;
            @$commune = $_POST['commune'] ;
            @$nationalite = $_POST['nationalite'] ;
            @$profession = $_POST['profession'] ;
            @$email = $_POST['email'] ;
            @$telephone = $_POST['telephone'] ;
            @$details_faits = $_POST['details_faits'] ;
            @$envoyer = $_POST['envoyer'] ;

            if(isset($envoyer) ){
                
                if(!empty($prenom) &&  !empty($nom) ){
                  //INSERTION DANS LA TABLE PERSONNE
                  $sql_personne = "INSERT INTO personne (nom,prenom,date_naissance,lieu_naissance,adresse,telephone,mail,situation_familliale,nationalite,profession,nom_epoux,region,departement,commune,sexe) 
                  VALUES ('$nom','$prenom','$date_naissance','$lieu_naissance','$adresse','$telephone','$email','$situation_familliale','$nationalite','$profession','$epoux','$region','$departement','$commune','$sexe')" ;
                  $sql_personne1 = "SELECT id_personne FROM personne WHERE nom ='$nom' AND prenom='$prenom' AND date_naissance = '$date_naissance' AND lieu_naissance = '$lieu_naissance' AND adresse='$adresse'
                  AND telephone= '$telephone' AND mail = '$email' AND situation_familliale = '$situation_familliale' AND nationalite = '$nationalite' AND profession = '$profession' AND nom_epoux= '$epoux' " ;
                  $requete_personne = mysqli_query($conn , $sql_personne) ;
                  $requete_personne1 = mysqli_query($conn , $sql_personne1) ;
                  //INSERTION DANS LA TABLE PLAINTE
                  $resultat = mysqli_fetch_assoc ($requete_personne1) ;
                  $id_personne = $resultat['id_personne'] ;
                  $sql_plainte = "INSERT INTO plainte (id_personne,nature_plainte,qualite_plainte,faits,date_faits) VALUES ('$id_personne','$nature_plainte','$qualite_plainte','$details_faits','$date_faits')" ;
                  $requete_plainte = mysqli_query($conn , $sql_plainte) ;
                  if ($requete_personne && $requete_plainte){
                      echo "insertion réussie" ;
                  }
                  else mysqli_error($conn);

                }
                else {
                    echo "veuillez remplir les champs obligatoires" ;
                }


            }
            

?>


<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon-gendarmerie.png">
    <title>Brigade Numérique | Gendarmerie</title>
    <!-- This page CSS -->
    <link href="../../assets/libs/jquery-steps/jquery.steps.css" rel="stylesheet">
    <link href="../../assets/libs/jquery-steps/steps.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- FAQ CSS -->
    <link rel="stylesheet" href="faq/style.css">
	<!-- Contact CSS -->
	<link rel="stylesheet" href="contact/main.css">
    <!-- Font -->
    <style>
        @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@500&display=swap');
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <a href="index.html" class="logo">
                            <!-- Logo icon -->
                            <b class="logo-icon">
                                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Logo icon -->
                                <img src="../../assets/images/logo-crogend.jfif" alt="homepage" width="30%"/>
                            </b>
                            <!--End Logo icon -->
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->    
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right m-auto">
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                                <i class="mdi mdi-menu font-24"></i>
                            </a>
                        </li> 
                        <li class="align-self-center">
                                <h1 class="card-title font-weight-bold" style="font-family:'IBM Plex Sans', sans-serif;"> Brigade Numérique</h1>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Barre de recherche -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left">
                        <li class="nav-item search-box">
                            <a class="nav-link waves-effect waves-dark" href="javascript:void(0)" >
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-magnify font-20 mr-1"></i>
                                    <div class="ml-1 d-none d-sm-block">
                                        <span>Rechercher</span>
                                    </div>
                                </div>
                            </a>
                            <form class="app-search position-absolute">
                                <input type="text" class="form-control" placeholder="Rechercher ...">
                                <a class="srh-btn">
                                    <i class="ti-close"></i>
                                </a>
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side Logo Gendarmerie -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- LOGO Gendarmerie -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a href="javascript:void(0)" class="logo">
                                <!-- Logo icon -->
                                <b class="logo-icon">
                                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                    <!-- Logo icon -->
                                    <img class="float-right" src="../../assets/images/favicon-gendarmerie.png" alt="homepage" width="30%"/>
                                </b>
                                <!--End Logo icon -->
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End LOGO CROGEND -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="index.html" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Accueil </span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link two-column waves-effect waves-dark" href="pre-plainte-form.html" aria-expanded="false"><i class="mdi mdi-fingerprint"></i><span class="hide-menu">Pré-Plainte </span></a></li>
                        <li class="sidebar-item mega-dropdown"> <a class="sidebar-link waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-alert-outline"></i><span class="hide-menu">Signaler un cas </span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-comment-question-outline"></i><span class="hide-menu">FAQ</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-email-outline"></i><span class="hide-menu">Contact</span></a></li>
                
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
         <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pré-Plainte en Ligne</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index.html">Accueil</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Signaler un cas</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Section -->
                <!-- ============================================================== -->
                <div class="row banner-plainte">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body wizard-content">
                                <h4 class="card-title">Identité </h4>
                                <h6 class="card-subtitle">Les champs suivis de (*) sont obligatoires</h6>
                                <form class="form-horizontal" method="POST" action="pre-plainte-form.php">
                                    <hr>
                                    <h4 class="card-title">Les informations vous concernant </h4>
                                     <h6 class="card-subtitle"><em>Seule la victime, ou son représentant légal le cas échéant, peut établir cette déclaration.</em></h6>
                                    <div class="card-body">
                                               <!-- <h4 class="card-title">Etat Civil</h4> -->
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Nature de votre plainte: (*)</label>
                                                    <div class="col-md-3">
                                                        <select class="form-control custom-select" name="nature_plainte">
                                                            <option value="">Precisez la nature de votre plainte (*)</option>
                                                            <option value="">Atteinte aux biens</option>
                                                            <option value="">Atteinte aux personne</option>
                                                        </select>
                                                    </div>
                                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Date / Heure des faits: </label>
                                                    <div class="col-sm-3">
                                                        <input type="datetime-local" class="form-control" value="2008-05-13T22:33:00" name="date_faits">
                                                    </div>
                                                </div>
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Sexe: (*)</label>
                                        <div class="col-md-3">
                                            <select class="form-control custom-select" name="sexe">
                                                <option value="">Sexe (*)</option>
                                                <option value="Masculin">Masculin</option>
                                                <option value="Feminin">Feminin</option>
                                            </select>
                                        </div>
                                        <label class="control-label text-right col-md-3">vous déposez une plainte en qualité de (*)</label>
                                        <div class="col-md-3">
                                            <select class="form-control custom-select" name="qualite_plainte">
                                                <option value="">vous déposez une plainte en qualité de (*)</option>
                                                <option value="Victime">Victime</option>
                                                <option value="Représentant légal d'une personne physique">Représentant légal d'une personne physique</option>
                                                <option value="Représentant légal d'une personne moral">Représentant légal d'une personne moral</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Prénom(s)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="fname" placeholder="Votre prénom" name="prenom">
                                            </div>
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Nom</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="lname" placeholder="Votre nom" name="nom">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Situation Familiale(*)</label>
                                            <div class="col-md-3">
                                                <select class="form-control custom-select" name="situation_familliale">
                                                    <option value="">Choisissez</option>
                                                    <option value="Célibataire">Célibataire</option>
                                                    <option value="Marié(e)">Marié(e)</option>
                                                    <option value="Divorcé">Divorcé(e)</option>
                                                    <option value="Veuf">Veuf(ve)</option>
                                                </select>
                                            </div>
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Nom d'époux(se)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="nepoux" placeholder="Nom d'époux(se)" name="epoux">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Date de naissance</label>
                                            <div class="col-sm-3">
                                                <input type="date" class="form-control" value="2018-05-13" name="date_naissance">
                                            </div>
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Lieu de naissance: </label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="lnaiss" placeholder="Lieu de naissance" name="lieu_naissance">
                                            </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <label for="adresse" class="col-sm-3 text-right control-label col-form-label">Adresse</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="adresse" placeholder="Votre adresse" name="adresse">
                                            </div>
                                            <label class="control-label text-right col-md-3">Région</label>
                                            <div class="col-md-3">
                                                <select class="form-control custom-select" name="region">
                                                    <option value="">Choisissez votre Région</option>
                                                    <option value="">Dakar</option>
                                                    <option value="">Diourbel</option>
                                                    <option value="">Fatick</option>
                                                    <option value="">Kaffrine</option>
                                                    <option value="">Kaolack</option>
                                                    <option value="">Kédougou</option>
                                                    <option value="">Kolda</option>
                                                    <option value="">Louga</option>
                                                    <option value="">Matam</option>
                                                    <option value="">Fatick</option>
                                                    <option value="">Saint-Louis</option>
                                                    <option value="">Sédhiou</option>
                                                    <option value="">Tambacounda</option>
                                                    <option value="">Thiès</option>
                                                    <option value="">Ziguinchor</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Département : </label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="dept" placeholder="Département :" name="departement">
                                            </div>
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Commune : </label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="com" placeholder="Commune :" name="commune">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Nationalité(*)</label>
                                            <div class="col-md-3">
                                                <select id="Personne_Nationalite" 0="" name="nationalite">
                                                    <option value="-1">Choisissez</option>
                                                    <option value="Française ">Française</option>
                                                    <option value="Indéterminée ">Indéterminée</option>
                                                    <option value="Afghane">Afghane</option>
                                                    <option value="Albanaise">Albanaise</option>
                                                    <option value="Algerienne">Algerienne</option>
                                                    <option value="Allemande">Allemande</option>
                                                    <option value="Americaine">Americaine</option>
                                                    <option value="Andorrane">Andorrane</option>
                                                    <option value="Angolaise">Angolaise</option>
                                                    <option value="Antiguayenne">Antiguayenne</option>
                                                    <option value="Apatride">Apatride</option>
                                                    <option value="Argentine">Argentine</option>
                                                    <option value="Armenienne">Armenienne</option>
                                                    <option value="Australienne">Australienne</option>
                                                    <option value="Autrichienne">Autrichienne</option>
                                                    <option value="Azerbaidjanaise">Azerbaidjanaise</option>
                                                    <option value="Bahameenne">Bahameenne</option>
                                                    <option value="Bahreinienne">Bahreinienne</option>
                                                    <option value="Bangladaise">Bangladaise</option>
                                                    <option value="Barbadienne">Barbadienne</option>
                                                    <option value="Belge">Belge</option>
                                                    <option value="Belizienne">Belizienne</option>
                                                    <option value="Beninoise">Beninoise</option>
                                                    <option value="Bhoutanaise">Bhoutanaise</option>
                                                    <option value="Bielorusse">Bielorusse</option>
                                                    <option value="Birmane">Birmane</option>
                                                    <option value="Bissao guineenne">Bissao guineenne</option>
                                                    <option value="Bolivienne">Bolivienne</option>
                                                    <option value="Bosnienne">Bosnienne</option>
                                                    <option value="Botswanaise">Botswanaise</option>
                                                    <option value="Bresilienne">Bresilienne</option>
                                                    <option value="Britannique">Britannique</option>
                                                    <option value="Bruneienne">Bruneienne</option>
                                                    <option value="Bulgare">Bulgare</option>
                                                    <option value="Burkinabee">Burkinabee</option>
                                                    <option value="Burundaise">Burundaise</option>
                                                    <option value="Cambodgienne">Cambodgienne</option>
                                                    <option value="Camerounaise">Camerounaise</option>
                                                    <option value="Canadienne">Canadienne</option>
                                                    <option value="Cap verdienne">Cap verdienne</option>
                                                    <option value="Centrafricaine">Centrafricaine</option>
                                                    <option value="Chilienne">Chilienne</option>
                                                    <option value="Chinoise">Chinoise</option>
                                                    <option value="Christophienne">Christophienne</option>
                                                    <option value="Chypriote">Chypriote</option>
                                                    <option value="Colombienne">Colombienne</option>
                                                    <option value="Comorienne">Comorienne</option>
                                                    <option value="Congolaise">Congolaise</option>
                                                    <option value="Costaricaine">Costaricaine</option>
                                                    <option value="Croate">Croate</option>
                                                    <option value="Cubaine">Cubaine</option>
                                                    <option value="Danoise">Danoise</option>
                                                    <option value="Djiboutienne">Djiboutienne</option>
                                                    <option value="Dominicaine">Dominicaine</option>
                                                    <option value="Dominiquaise">Dominiquaise</option>
                                                    <option value="Egyptienne">Egyptienne</option>
                                                    <option value="Emirienne">Emirienne</option>
                                                    <option value="Equato guineenne">Equato guineenne</option>
                                                    <option value="Equatorienne">Equatorienne</option>
                                                    <option value="Erythreenne">Erythreenne</option>
                                                    <option value="Espagnole">Espagnole</option>
                                                    <option value="Estonienne">Estonienne</option>
                                                    <option value="Ethiopienne">Ethiopienne</option>
                                                    <option value="Fidjienne">Fidjienne</option>
                                                    <option value="Finlandaise">Finlandaise</option>
                                                    <option value="Française">Française</option>
                                                    <option value="Gabonaise">Gabonaise</option>
                                                    <option value="Gambienne">Gambienne</option>
                                                    <option value="Georgienne">Georgienne</option>
                                                    <option value="Ghaneenne">Ghaneenne</option>
                                                    <option value="Grecque">Grecque</option>
                                                    <option value="Grenadienne">Grenadienne</option>
                                                    <option value="Guatemalteque">Guatemalteque</option>
                                                    <option value="Guineenne">Guineenne</option>
                                                    <option value="Guyanienne">Guyanienne</option>
                                                    <option value="Haitienne">Haitienne</option>
                                                    <option value="Hondurienne">Hondurienne</option>
                                                    <option value="Hongroise">Hongroise</option>
                                                    <option value="Indéterminée">Indéterminée</option>
                                                    <option value="Indienne">Indienne</option>
                                                    <option value="Indonesienne">Indonesienne</option>
                                                    <option value="Irakienne">Irakienne</option>
                                                    <option value="Iranienne">Iranienne</option>
                                                    <option value="Irlandaise">Irlandaise</option>
                                                    <option value="Islandaise">Islandaise</option>
                                                    <option value="Israelienne">Israelienne</option>
                                                    <option value="Italienne">Italienne</option>
                                                    <option value="Ivoirienne">Ivoirienne</option>
                                                    <option value="Jamaiquaine">Jamaiquaine</option>
                                                    <option value="Japonaise">Japonaise</option>
                                                    <option value="Jordanienne">Jordanienne</option>
                                                    <option value="Kazakhstanaise">Kazakhstanaise</option>
                                                    <option value="Kenyane">Kenyane</option>
                                                    <option value="Kirghize">Kirghize</option>
                                                    <option value="Kiribatienne">Kiribatienne</option>
                                                    <option value="Kosovare">Kosovare</option>
                                                    <option value="Koweitienne">Koweitienne</option>
                                                    <option value="Laotienne">Laotienne</option>
                                                    <option value="Lesothienne">Lesothienne</option>
                                                    <option value="Lettone">Lettone</option>
                                                    <option value="Libanaise">Libanaise</option>
                                                    <option value="Liberienne">Liberienne</option>
                                                    <option value="Libyenne">Libyenne</option>
                                                    <option value="Liechtensteinoise">Liechtensteinoise</option>
                                                    <option value="Lituanienne">Lituanienne</option>
                                                    <option value="Luxembourgeoise">Luxembourgeoise</option>
                                                    <option value="Macedonienne">Macedonienne</option>
                                                    <option value="Malaisienne">Malaisienne</option>
                                                    <option value="Malawienne">Malawienne</option>
                                                    <option value="Maldivienne">Maldivienne</option>
                                                    <option value="Malgache">Malgache</option>
                                                    <option value="Malienne">Malienne</option>
                                                    <option value="Maltaise">Maltaise</option>
                                                    <option value="Marinaise">Marinaise</option>
                                                    <option value="Marocaine">Marocaine</option>
                                                    <option value="Marshallaise">Marshallaise</option>
                                                    <option value="Mauricienne">Mauricienne</option>
                                                    <option value="Mauritanienne">Mauritanienne</option>
                                                    <option value="Mexicaine">Mexicaine</option>
                                                    <option value="Micronesienne">Micronesienne</option>
                                                    <option value="Moldave">Moldave</option>
                                                    <option value="Monegasque">Monegasque</option>
                                                    <option value="Mongole">Mongole</option>
                                                    <option value="Montenegrine">Montenegrine</option>
                                                    <option value="Mozambicaine">Mozambicaine</option>
                                                    <option value="Namibienne">Namibienne</option>
                                                    <option value="Nauruane">Nauruane</option>
                                                    <option value="Neerlandaise">Neerlandaise</option>
                                                    <option value="Neo zelandaise">Neo zelandaise</option>
                                                    <option value="Nepalaise">Nepalaise</option>
                                                    <option value="Nicaraguayenne">Nicaraguayenne</option>
                                                    <option value="Nigeriane">Nigeriane</option>
                                                    <option value="Nigerienne">Nigerienne</option>
                                                    <option value="Nord coreenne">Nord coreenne</option>
                                                    <option value="Norvegienne">Norvegienne</option>
                                                    <option value="Omanaise">Omanaise</option>
                                                    <option value="Ougandaise">Ougandaise</option>
                                                    <option value="Ouzbeke">Ouzbeke</option>
                                                    <option value="Pakistanaise">Pakistanaise</option>
                                                    <option value="Palaoise">Palaoise</option>
                                                    <option value="Palestinienne">Palestinienne</option>
                                                    <option value="Panameenne">Panameenne</option>
                                                    <option value="Papouasienne">Papouasienne</option>
                                                    <option value="Paraguayenne">Paraguayenne</option>
                                                    <option value="Peruvienne">Peruvienne</option>
                                                    <option value="Philippine">Philippine</option>
                                                    <option value="Polonaise">Polonaise</option>
                                                    <option value="Portugaise">Portugaise</option>
                                                    <option value="Qatarienne">Qatarienne</option>
                                                    <option value="Roumaine">Roumaine</option>
                                                    <option value="Russe">Russe</option>
                                                    <option value="Rwandaise">Rwandaise</option>
                                                    <option value="Sainte lucienne">Sainte lucienne</option>
                                                    <option value="Salomonaise">Salomonaise</option>
                                                    <option value="Salvadorienne">Salvadorienne</option>
                                                    <option value="Samoenne">Samoenne</option>
                                                    <option value="Santomeenne">Santomeenne</option>
                                                    <option value="Saoudienne">Saoudienne</option>
                                                    <option value="Senegalaise">Senegalaise</option>
                                                    <option value="Serbe">Serbe</option>
                                                    <option value="Seychelloise">Seychelloise</option>
                                                    <option value="Sierra leonaise">Sierra leonaise</option>
                                                    <option value="Singapourienne">Singapourienne</option>
                                                    <option value="Slovaque">Slovaque</option>
                                                    <option value="Slovene">Slovene</option>
                                                    <option value="Somalienne">Somalienne</option>
                                                    <option value="Soudanaise">Soudanaise</option>
                                                    <option value="Sri lankaise">Sri lankaise</option>
                                                    <option value="Sud africaine">Sud africaine</option>
                                                    <option value="Sud coreenne">Sud coreenne</option>
                                                    <option value="Sud soudanaise">Sud soudanaise</option>
                                                    <option value="Suedoise">Suedoise</option>
                                                    <option value="Suisse">Suisse</option>
                                                    <option value="Surinamaise">Surinamaise</option>
                                                    <option value="Swazie">Swazie</option>
                                                    <option value="Syrienne">Syrienne</option>
                                                    <option value="Tadjike">Tadjike</option>
                                                    <option value="Taiwanaise">Taiwanaise</option>
                                                    <option value="Tanzanienne">Tanzanienne</option>
                                                    <option value="Tchadienne">Tchadienne</option>
                                                    <option value="Tcheque">Tcheque</option>
                                                    <option value="Thailandaise">Thailandaise</option>
                                                    <option value="Timoraise">Timoraise</option>
                                                    <option value="Togolaise">Togolaise</option>
                                                    <option value="Tonguienne">Tonguienne</option>
                                                    <option value="Trinidadienne">Trinidadienne</option>
                                                    <option value="Tunisienne">Tunisienne</option>
                                                    <option value="Turkmene">Turkmene</option>
                                                    <option value="Turque">Turque</option>
                                                    <option value="Tuvaluane">Tuvaluane</option>
                                                    <option value="Ukrainienne">Ukrainienne</option>
                                                    <option value="Uruguayenne">Uruguayenne</option>
                                                    <option value="Vanuatuane">Vanuatuane</option>
                                                    <option value="Vaticane">Vaticane</option>
                                                    <option value="Venezuelienne">Venezuelienne</option>
                                                    <option value="Vietnamienne">Vietnamienne</option>
                                                    <option value="Vincentaise">Vincentaise</option>
                                                    <option value="Yemenite">Yemenite</option>
                                                    <option value="Zambienne">Zambienne</option>
                                                    <option value="Zimbabweenne">Zimbabweenne</option>
                                                </select>
                                            </div>
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Profession : </label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="com" placeholder="Profession :" name="profession">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email1" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                            <div class="col-sm-3">
                                                <input type="email" class="form-control" id="email1" placeholder="Votre adresse email" name="email">
                                            </div>
                                            <label for="tel1" class="col-sm-3 text-right control-label col-form-label">Téléphone</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="tel1" placeholder="Votre numéro de téléphone" name="telephone">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Racontez-nous en détails les faits (*)</label>
                                        <textarea class="form-control" rows="5" name="details_faits"></textarea>
                                    </div>
                                    
                                    <div class="form-group text-center">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary mb-2" name="envoyer" value="SOUMETTRE CETTE DEPOSITION A LA BRIGADE NUMERIQUE" />
                                             
                                        </div>
                                    <hr>
                            
                                </form>
                            </div>
                        </div>
                    </div>
                </div>  
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <div class="footer" style="background-color: #001933;">
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-12 col-md-12">
                        <div class="row p-5" >
                            <div class="col-xl-4 col-md-4">
                                <div class="row">
                                    <div class="col-xl-4 col-md-4 col-sm-6">
                                        <img src="../../assets/images/favicon-gendarmerie.png" alt="" width="100%">
                                    </div>
                                    <div class="col-xl-8 col-md-8 col-sm-6">
                                        <h4>Haut Commandement de la Gendarmerie Nationale</h4>
                                        <h6>Centre de Recherches et des Opérations de la Gendarmerie Nationale - CROGEND</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-4 col-md-4 col-sm-4"></div> 
                                    <div class="col-xl-8 col-md-8 col-sm-8">
                                        <br>
                                        <hr>
                                        <h4 class="mb-3">Brigade Numérique</h4>
                                        <p><a href="error-400.html">A Propos</a></p>
                                        <p><a href="faq.html">FAQ</a></p>
                                        <p><a href="error-400.html">Termes & Conditions</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-4 ml-5 pl-5">
                                <h4>Liens Utiles</h4>
                                <p><a href="https://www.sec.gouv.sn/">Gouvernement Sénégal</a></p>
                                <p><a href="https://forcesarmees.sec.gouv.sn/">Ministère des Forces Armées</a></p>
                                <p><a href="https://interieur.sec.gouv.sn/">Ministère de l'intérieur</a></p>
                                <p><a href="https://forcesarmees.sec.gouv.sn/">Ministère de la justice</a></p>
                                <p><a href="https://forcesarmees.sec.gouv.sn/">Armée Sénégalaise</a></p>
                                <p><a href="https://www.gendarmerie.sn/">Gendarmerie Nationale</a></p>
                            </div>
                            <div class="col-xl-3 col-md-3">
                                <h4 class="mb-1">Numéro Vert Gendarmerie :</h4>
                                <h4 style="color: #6a7a8c;">800 00 20 20</h4>
                                <br>
                                <button class="btn waves-effect btn-circle waves-light" type="button"> <i class="fab fa-facebook"></i> </button>
                                <button class="btn waves-effect btn-circle waves-light" type="button"> <i class="fab fa-twitter"></i> </button>
                                <button class="btn waves-effect btn-circle waves-light" type="button"> <i class="fab fa-whatsapp"></i> </button>
                                
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                &copy;2021 Gendarmerie Nationale Sénégalaise. Tous droits réservés.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.js"></script>
    <script src="../../assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="../../assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- ChatBot -->
    <script src="//code.tidio.co/h0wcezraw5v1k3cjeyichxyv1yzlcfsg.js" async></script>
</body>

</html>